package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/jumbo/petsrv/petproto"
)

func (s *petServiceServer) GetPet(ctx context.Context, in *petproto.GetPetRequest) (*petproto.Pet, error) {
	log.Info("GetPet was called, wit ID ", in.Id)
	pet := petproto.Pet{}
	row := db.QueryRow("SELECT rawjson FROM pet.pet WHERE id = $1;", in.Id)
	var rawJSON string
	err := row.Scan(&rawJSON)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		log.Error("can't get pet, ", err)
		return nil, errors.New(fmt.Sprint("Can not get Pet by ID: ", in.Id, " see log for details"))
	}

	err = json.Unmarshal([]byte(rawJSON), &pet)
	if err != nil {
		log.Error(err)
		return nil, errors.New(fmt.Sprint("Can not get Pet by ID: ", in.Id, " see log for details"))
	}

	log.Debug("returning a Pet: ", pet)
	return &pet, nil
}

func (s *petServiceServer) DeletePet(ctx context.Context, in *petproto.DeletePetRequest) (*petproto.OkResponse, error) {
	log.Info("DeletePet was called, wit ID ", in.Id)
	res, err := db.Exec("DELETE FROM pet.pet WHERE id = $1;", in.Id)
	count, _ := res.RowsAffected()
	if err != nil || count < 1 {
		if err == sql.ErrNoRows {
			return &petproto.OkResponse{Ok: false}, sql.ErrNoRows
		}
		log.Error("can't delete pet, ", err)
		return &petproto.OkResponse{Ok: false}, errors.New(fmt.Sprint("Can not delete Pet by ID: ", in.Id, " see log for details"))
	}

	return &petproto.OkResponse{Ok: true}, nil
}

func (s *petServiceServer) AddPet(ctx context.Context, in *petproto.AddPetRequest) (*petproto.OkResponse, error) {
	log.Info("AddPet was called, wit ID ", in.Pet.Id)
	rawBytes, err := json.Marshal(in.Pet)
	if err != nil {
		log.Error("Can't marshal pet to json, ", err)
		return &petproto.OkResponse{Ok: false}, errors.New(fmt.Sprint("Can not add a new Pet: ", in.Pet, " see log for details"))
	}
	json := string(rawBytes)
	res, err := db.Exec("INSERT INTO pet.pet (id, petstatus, rawjson) values ($1, $2, $3);", in.Pet.Id, in.Pet.Status, json)
	if err != nil {
		log.Error("Can't add pet, ", err)
		return &petproto.OkResponse{Ok: false}, errors.New(fmt.Sprint("Can not add Pet ", in.Pet, " see log for details"))
	}
	count, _ := res.RowsAffected()
	var result petproto.OkResponse
	if count > 0 {
		result = petproto.OkResponse{Ok: true}
	} else {
		result = petproto.OkResponse{Ok: false}
	}
	return &result, nil
}

func UpdatePet(ctx context.Context, in *petproto.UpdatePetRequest) (*petproto.OkResponse, error) {
	log.Info("UpdatePet was called, wit ID ", in.Pet.Id)
	rawBytes, err := json.Marshal(in.Pet)
	if err != nil {
		log.Error("Can't marshal Pet to json, ", err)
		return &petproto.OkResponse{Ok: false}, errors.New(fmt.Sprint("Can not update a Pet: ", in.Pet, " see log for details"))
	}
	json := string(rawBytes)
	res, err := db.Exec("UPDATE pet.pet SET rawjson = $1 WHERE id = $2);", json, in.Pet.Id)
	if err != nil {
		log.Error("Can't update pet, ", err.Error())
		return &petproto.OkResponse{Ok: false}, errors.New(fmt.Sprint("Can not update Pet ", in.Pet, " see log for details"))
	}
	count, _ := res.RowsAffected()
	var result petproto.OkResponse
	if count > 0 {
		result = petproto.OkResponse{Ok: true}
	} else {
		result = petproto.OkResponse{Ok: false}
	}
	return &result, nil
}
