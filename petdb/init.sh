#!/bin/bash
set -e

echo "Init database"
psql -Upostgres -f /db.sql;
psql -d petstore -U postgres -f /schema.sql
psql -d petstore -U postgres -f /data.sql
echo "Database init complete"
