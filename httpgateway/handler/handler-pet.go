package handler

import (
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/jumbo/httpgateway/petproto"
	"google.golang.org/grpc"
)

const (
	petSrvEndpoint = "petsrv:8081"
)

var conn *grpc.ClientConn
var petClient petproto.PetServiceClient

func init() {
	var err error
	conn, err = grpc.Dial(petSrvEndpoint, grpc.WithInsecure())
	if err != nil {
		log.Error("can't communicate to Pet service")
	}

	petClient = petproto.NewPetServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	readinessProbe(ctx, petClient)
	cancel()
}

func Close() {
	conn.Close()
}

func GetPetById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idStr, ok := vars["petId"]
	if !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), 1*time.Minute)
	defer cancel()
	request := petproto.GetPetRequest{
		Id: int32(id),
	}
	log.Info("calling Pet service - get Pet by ID = ", id)

	pet, err := petClient.GetPet(ctx, &request)
	if err != nil || pet == nil {
		if strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		log.Error("error while getting Pet by ID ", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(pet)
}

func UpdatePetById(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Error("error while updating Pet, in reading request body ", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	rawJSON := string(b)
	log.Info("update pet, request body: ", rawJSON)

	pet := petproto.Pet{}
	err = json.Unmarshal([]byte(rawJSON), &pet)
	if err != nil {
		log.Error("error while updating Pet, unmarshalling error: ", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), 1*time.Minute)
	defer cancel()
	request := petproto.UpdatePetRequest{
		Pet: &pet,
	}
	log.Info("calling Pet service - update Pet ", pet.Id)
	resp, err := petClient.UpdatePet(ctx, &request)
	if err != nil || !resp.Ok {
		log.Error("error while updating Pet ", err, resp)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	log.Debug("updating Pet - success, id = ", pet.Id)

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode("{'result': 'success'}")
}

func DeletePetById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idStr, ok := vars["petId"]
	if !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), 1*time.Minute)
	defer cancel()
	request := petproto.DeletePetRequest{
		Id: int32(id),
	}
	log.Info("calling Pet service - delete Pet by ID = ", id)

	resp, err := petClient.DeletePet(ctx, &request)
	if err != nil || !resp.Ok {
		if strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		log.Error("error while deleting Pet by ID ", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode("{'result': 'success'}")
}

func AddPet(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Error("error while add Pet, in reading request body ", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	rawJSON := string(b)
	log.Debug("add pet, request body: ", rawJSON)

	pet := petproto.Pet{}
	err = json.Unmarshal([]byte(rawJSON), &pet)
	if err != nil {
		log.Error("error while adding Pet, unmarshalling ", err.Error())
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), 1*time.Minute)
	defer cancel()
	request := petproto.AddPetRequest{
		Pet: &pet,
	}
	log.Info("calling Pet service - add Pet ", pet.Id)
	resp, err := petClient.AddPet(ctx, &request)
	if err != nil || !resp.Ok {
		log.Error("error while adding Pet ", err, resp)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	log.Debug("adding Pet - success, id = ", pet.Id)

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode("{'result': 'success'}")
}

func readinessProbe(ctx context.Context, petClient petproto.PetServiceClient) {
	time.Sleep(10 * time.Second)
	log.Info("readiness probe: calling Pet service - get Pet by ID = 0")
	request := petproto.GetPetRequest{
		Id: 0,
	}
	response, err := petClient.GetPet(ctx, &request)
	if err != nil {
		log.Error("readiness probe failed: could not get Pet by ID: ", err)
	}
	if response != nil {
		log.Info("readiness probe succeeded ", response)
	}
}
