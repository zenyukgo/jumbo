package main

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/jumbo/httpgateway/handler"
)

const (
	httpPort = ":8080"
)

func main() {
	defer close()
	router := mux.NewRouter()
	router.HandleFunc("/pet/{petId:[0-9]+}", handler.GetPetById).Methods("GET")
	router.HandleFunc("/pet/{petId:[0-9]+}", handler.UpdatePetById).Methods("POST")
	router.HandleFunc("/pet/{petId:[0-9]+}", handler.DeletePetById).Methods("DELETE")
	router.HandleFunc("/pet", handler.AddPet).Methods("POST")
	log.Info("http-gateway service is ready on port ", httpPort)
	http.ListenAndServe(httpPort, router)
	select {}
}

func close() {
	handler.Close()
}
